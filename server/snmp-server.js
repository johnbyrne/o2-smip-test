// Load average oids
var oneMinuteLoadAverage = [1, 3, 6, 1, 4, 1, 2021, 10, 1, 3, 1];
var fiveMinuteLoadAverage = [1, 3, 6, 1, 4, 1, 2021, 10, 1, 3, 2];
var fifteenMinuteLoadAverage = [1, 3, 6, 1, 4, 1, 2021, 10, 1, 3, 3];

// Total oids
var totalRAMInMachine = [1, 3, 6, 1, 4, 1, 2021, 4, 5, 0];
var totalRAMused = [1, 3, 6, 1, 4, 1, 2021, 4, 6, 0];
var totalRAMFree = [1, 3, 6, 1, 4, 1, 2021, 4, 11, 0];	
var totalRAMShared = [1, 3, 6, 1, 4, 1, 2021, 4, 13, 0];	
var totalRAMBuffered = [1, 3, 6, 1, 4, 1, 2021, 4, 14, 0];	
var totalCachedMemory = [1, 3, 6, 1, 4, 1, 2021, 4, 15, 0];	

// This is a more challenging one,  When SNMP-Walked this will TCP Connection State, Local Address, Local Port, Foreign Address, and Foreign Port,  (See http://net-snmp, sourceforge, net/docs/mibs/tcp, html)
var tcpNetworkConnections = [1, 3, 6, 1, 2, 1, 6, 13, 1];

module.exports = {
	getOneMinuteLoadAverage: function(session, callback) { getSnmpSessionByOid(session, oneMinuteLoadAverage, callback); },
	getFiveMinuteLoadAverage: function(session, callback) { getSnmpSessionByOid(session, fiveMinuteLoadAverage, callback); },
	getFifteenMinuteLoadAverage: function(session, callback) { getSnmpSessionByOid(session, fifteenMinuteLoadAverage, callback); },
	getTotalRAMInMachine: function(session, callback) { getSnmpSessionByOid(session, totalRAMInMachine, callback); },
	getTotalRAMused: function(session, callback) { getSnmpSessionByOid(session, totalRAMused, callback); },
	getTotalRAMFree: function(session, callback) { getSnmpSessionByOid(session, totalRAMFree, callback); },
	getTotalRAMShared: function(session, callback) { getSnmpSessionByOid(session, totalRAMShared, callback); },
	getTotalRAMBuffered: function(session, callback) { getSnmpSessionByOid(session, totalRAMBuffered, callback); },
	getTotalCachedMemory: function(session, callback) { getSnmpSessionByOid(session, totalCachedMemory, callback); }
};

function getSnmpSessionByOid(session, oid, callback) {
	session.get({ oid: oid }, function (error, varbinds) {
	    if (error) {
	        console.log('Fail :(');
	        console.log(JSON.stringify(error, null, 4));
	    } else {
	        console.log(varbinds[0].oid + ' = ' + varbinds[0].value + ' (' + varbinds[0].type + ')');
	        callback(varbinds);
	    }
	});
}