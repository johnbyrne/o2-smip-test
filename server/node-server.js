module.exports = {
	initialise: function(app, express, path, bodyParser, session, snmpServer) {
		app.use(express.static(path.join(__dirname, '../app')));
		app.use(bodyParser());
		app.get('/api/oneMinuteLoadAverage', function (req, res) {
			snmpServer.getOneMinuteLoadAverage(session, function(data) {
				res.json({ data: data });
			});
		});
		app.get('/api/fiveMinuteLoadAverage', function (req, res) {
			snmpServer.getFiveMinuteLoadAverage(session, function(data) {
				res.json({ data: data });
			});
		});
		app.get('/api/fifteenMinuteLoadAverage', function (req, res) {
			snmpServer.getFifteenMinuteLoadAverage(session, function(data) {
				res.json({ data: data });
			});
		});
		app.get('/api/totalRAMInMachine', function (req, res) {
			snmpServer.getTotalRAMInMachine(session, function(data) {
				res.json({ data: data });
			});
		});
		app.get('/api/totalRAMused', function (req, res) {
			snmpServer.getTotalRAMused(session, function(data) {
				res.json({ data: data });
			});
		});
		app.get('/api/totalRAMFree', function (req, res) {
			snmpServer.getTotalRAMFree(session, function(data) {
				res.json({ data: data });
			});
		});
		app.get('/api/totalRAMShared', function (req, res) {
			snmpServer.getTotalRAMShared(session, function(data) {
				res.json({ data: data });
			});
		});
		app.get('/api/totalRAMBuffered', function (req, res) {
			snmpServer.getTotalRAMBuffered(session, function(data) {
				res.json({ data: data });
			});
		});
		app.get('/api/totalCachedMemory', function (req, res) {
			snmpServer.getTotalCachedMemory(session, function(data) {
				res.json({ data: data });
			});
		});
	}
};
