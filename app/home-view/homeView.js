'use strict';

angular.module('o2SmipApp.homeView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {
        templateUrl: 'home-view/homeView.html',
        controller: 'HomeViewCtrl'
    });
}])

.controller('HomeViewCtrl', ['$scope', 'SnmpService',
    function($scope, SnmpService) {
        var oneMinuteLoadAverageValues = [],
            fiveMinuteLoadAverageValues = [],
            fifteenMinuteLoadAverageValues = [],
            totalRamInMachineValues = [],
            totalRamUsedValues = [],
            totalRamFreeValues = [],
            totalRamSharedValues = [],
            totalRamBufferedValues = [],
            totalCachedMemoryValues = [];

        initialiseStatisticArray(oneMinuteLoadAverageValues);
        initialiseStatisticArray(fiveMinuteLoadAverageValues);
        initialiseStatisticArray(fifteenMinuteLoadAverageValues);
        initialiseStatisticArray(totalRamInMachineValues);
        initialiseStatisticArray(totalRamUsedValues);
        initialiseStatisticArray(totalRamFreeValues);
        initialiseStatisticArray(totalRamSharedValues);
        initialiseStatisticArray(totalRamBufferedValues);
        initialiseStatisticArray(totalCachedMemoryValues);

        function initialiseStatisticArray(array) {
            for (var i=0; i<60; i++) {
                array.push(Math.random() * 0);
            }
        }

        $scope.values1 = [];
        $scope.values2 = [];

        setInterval(function() {
            SnmpService.getOneMinuteLoadAverage($scope, oneMinuteLoadAverageValues);
            SnmpService.getFiveMinuteLoadAverage($scope, fiveMinuteLoadAverageValues);
            SnmpService.getFifteenMinuteLoadAverage($scope, fifteenMinuteLoadAverageValues);

            $scope.values1 = {data: [oneMinuteLoadAverageValues,
                fiveMinuteLoadAverageValues, fifteenMinuteLoadAverageValues]};
            
            SnmpService.getTotalRAMInMachine($scope, totalRamInMachineValues);
            SnmpService.getTotalRAMused($scope, totalRamUsedValues);
            SnmpService.getTotalRAMFree($scope, totalRamFreeValues);
            SnmpService.getTotalRAMShared($scope, totalRamSharedValues);
            SnmpService.getTotalRAMBuffered($scope, totalRamBufferedValues);
            SnmpService.getTotalCachedMemory($scope, totalCachedMemoryValues);

            $scope.values2 = {data: [totalRamInMachineValues, //totalRamUsedValues,
                /*totalRamSharedValues,*/ totalRamBufferedValues, totalCachedMemoryValues]};
        }, 500);
    }
]);
