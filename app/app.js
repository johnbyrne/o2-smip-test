'use strict';

angular.module('o2SmipApp', [
	'ngRoute',
	'o2SmipApp.homeView',
	'o2SmipApp.snmpService',
	'o2SmipApp.multiLineChartDirective'
])
.config(['$routeProvider', function($routeProvider) {
	$routeProvider.otherwise({redirectTo: '/home'});
}]);
