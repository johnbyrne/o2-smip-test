angular.module('o2SmipApp.multiLineChartDirective', [])

.directive('multilinechart', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
        	values: '='
        },
        template: '<div></div>',
        link: function(scope, element) {
        	var el = element,
        		drawn = false;

        	scope.$watch('values', function(newVal, oldVal) {
        		if (newVal.data !== undefined && newVal.data.length > 0) {
        			if (!drawn) {
        				drawChart(newVal.data, this);
        				drawn = true;
        			} else {
        				updateChart(newVal.data, this);
        			}
        		}
        	}.bind(el));

        	function updateChart(values, el) {
				var margin = {top: 10, right: 0, bottom: 0, left: 0},
				    width = 200 - margin.left - margin.right,
				    height = 100 - margin.top - margin.bottom;

        		var x = d3.scale.linear()
				    .range([0, width]);

				var y = d3.scale.linear()
				    .range([height, 0]);

				var line = d3.svg.line()
				    .x(function(d) { return x(d.id); })
				    .y(function(d) { return y(d.value); });

        		for (var i=0; i<values.length; i++) {
					for (var j=0; j<values[i].length; j++) {
						values[i][j] = { id: j, value: values[i][j] };
					}

					x.domain(d3.extent(values[i], function(d) { return d.id; }));
					y.domain(d3.extent(values[i], function(d) { return d.value; }));

        			var svg = d3.select(el[0]).transition();
        			// d3.select(svg.selectAll(".line")[0][i])
        			svg.select('.line-'+i)
			            .duration(750)
			            .attr("d", line(values[i]));

	    			for (var j=0; j<values[i].length; j++) {
						values[i][j] = values[i][j].value;
					}
        		}
        	}

        	function drawChart(values, el) {
				// Set the dimensions of the canvas / graph
				var margin = {top: 10, right: 0, bottom: 0, left: 0},
				    width = 200 - margin.left - margin.right,
				    height = 100 - margin.top - margin.bottom;

				// Parse the id / time
				var parseDate = d3.time.format("%d-%b-%y").parse;

				// Set the ranges
				var x = d3.scale.linear().range([0, width]);
				var y = d3.scale.linear().range([height, 0]);

				// Define the axes
				var xAxis = d3.svg.axis().scale(x)
				    .orient("bottom").ticks(10);

				var yAxis = d3.svg.axis().scale(y)
				    .orient("left").ticks(5);

				// Define the line
				var valueline = d3.svg.line()
				    .x(function(d) { return x(d.id); })
				    .y(function(d) { return y(d.value); });
				    
				// Adds the svg canvas
				var svg = d3.select(el[0])
				    .append("svg")
				        .attr("width", width + margin.left + margin.right)
				        .attr("height", height + margin.top + margin.bottom)
				    .append("g")
				        .attr("transform", 
				              "translate(" + margin.left + "," + margin.top + ")");

				for (var i=0; i<values.length; i++) {

					for (var j=0; j<values[i].length; j++) {
						values[i][j] = { id: j, value: values[i][j] };
					}

					// Scale the range of the data
					x.domain(d3.extent(values[i], function(d) { return d.id; }));
					y.domain([0, d3.max(values[i], function(d) { return d.value; })]);
					
					// Add the valueline path.
					svg.append("path")
					  .attr("class", "line line-" + i)
					  .attr("d", valueline(values[i]));

					for (var j=0; j<values[i].length; j++) {
						values[i][j] = values[i][j].value;
					}
				}

				// Add the X Axis
				// svg.append("g")
				//   .attr("class", "x axis")
				//   .attr("transform", "translate(0," + height + ")")
				//   .call(xAxis);

				// Add the Y Axis
				// svg.append("g")
				//   .attr("class", "y axis")
				//   .call(yAxis);
        	}
        }
    };
});