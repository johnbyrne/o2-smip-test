'use strict';

describe('o2SmipApp.snmpService spec', function() {
    describe('snmp get request test', function() {
        it('should get one minute load average data', inject(function($http, $httpBackend) {
          
          var $scope = {};

          $http.get('api/oneMinuteLoadAverage').success(function(data) {
              $scope.oneMinuteLoadAverage = data.data[0].value;
          });

          $httpBackend
            .when('GET', 'api/oneMinuteLoadAverage')
            .respond({
              "data": [
                {
                  "type": 4,
                  "value": "0.49",
                  "oid": [
                    1,
                    3,
                    6,
                    1,
                    4,
                    1,
                    2021,
                    10,
                    1,
                    3,
                    1
                  ],
                  "valueRaw": {
                    "type": "Buffer",
                    "data": [
                      48,
                      46,
                      52,
                      57
                    ]
                  },
                  "valueHex": "302e3439",
                  "requestId": 1072103424,
                  "receiveStamp": 1471528565251,
                  "sendStamp": 1471528565214
                }
              ]
            });

          $httpBackend.flush();

          expect($scope.oneMinuteLoadAverage).toEqual("0.49");
        }));
    });
});
