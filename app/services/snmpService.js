'use strict';

angular.module('o2SmipApp.snmpService', [])

.service('SnmpService', ['$http', function($http) {
    return {
        getOneMinuteLoadAverage: function($scope, array) {
            $http.get('api/oneMinuteLoadAverage').success(function(data) {
                $scope.oneMinuteLoadAverage = data.data[0].value;
                array.shift();
                array.push(parseFloat(data.data[0].value));
            });
        },
        getFiveMinuteLoadAverage: function($scope, array) {
            return $http.get('api/fiveMinuteLoadAverage').success(function(data) {
                $scope.fiveMinuteLoadAverage = data.data[0].value;
                array.shift();
                array.push(parseFloat(data.data[0].value));
            });
        },
        getFifteenMinuteLoadAverage: function($scope, array) {
            return $http.get('api/fifteenMinuteLoadAverage').success(function(data) {
                $scope.fifteenMinuteLoadAverage = data.data[0].value;
                array.shift();
                array.push(parseFloat(data.data[0].value));
            });
        },
        getTotalRAMInMachine: function($scope, array) {
            return $http.get('api/totalRAMInMachine').success(function(data) {
                $scope.totalRAMInMachine = data.data[0].value;
                array.shift();
                array.push(parseFloat(data.data[0].value));
            });
        },
        getTotalRAMused: function($scope, array) {
            return $http.get('api/totalRAMused').success(function(data) {
                $scope.totalRAMused = data.data[0].value;
                array.shift();
                array.push(parseFloat(data.data[0].value));
            });
        },
        getTotalRAMFree: function($scope, array) {
            return $http.get('api/totalRAMFree').success(function(data) {
                $scope.totalRAMFree = data.data[0].value;
                array.shift();
                array.push(parseFloat(data.data[0].value));
            });
        },
        getTotalRAMShared: function($scope, array) {
            return $http.get('api/totalRAMShared').success(function(data) {
                $scope.totalRAMShared = data.data[0].value;
                array.shift();
                array.push(parseFloat(data.data[0].value));
            });
        },
        getTotalRAMBuffered: function($scope, array) {
            return $http.get('api/totalRAMBuffered').success(function(data) {
                $scope.totalRAMBuffered = data.data[0].value;
                array.shift();
                array.push(parseFloat(data.data[0].value));
            });
        },
        getTotalCachedMemory: function($scope, array) {
            return $http.get('api/totalCachedMemory').success(function(data) {
                $scope.totalCachedMemory = data.data[0].value;
                array.shift();
                array.push(parseFloat(data.data[0].value));
            });
        }
    }
}]);
