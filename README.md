O2 SMIP TECH TEST
-----------------

Tech stack
----------

The tech stack uses Angular 1.4 and d3 on the client side, and Node, Express and node-snmp on the server side.

* Install with 'npm install' and 'bower install'
* Ensure gulp is installed globally with 'npm install -g gulp'
* Run with 'gulp dev' and visit localhost:8083
* Test with 'npm test'

![Untitled.png](https://bytebucket.org/johnbyrne/o2-smip-test/raw/267ab848e14d37fded20a476236aeff661807816/snmpMonitoring.png)
